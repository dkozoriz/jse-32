package ru.t1.dkozoriz.tm.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.IEndpointClient;
import ru.t1.dkozoriz.tm.dto.response.AbstractResultResponse;

import java.io.*;
import java.net.Socket;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractClient implements IEndpointClient {

    private String host = "localhost";

    private Integer port = 6060;

    private Socket socket;

    public AbstractClient(String host, Integer port) {
        this.host = host;
        this.port = port;
    }

    public AbstractClient(@NotNull final AbstractClient client) {
        this.host = client.getHost();
        this.port = client.getPort();
        this.socket = client.getSocket();
    }

    @NotNull
    @SneakyThrows
    protected <T> T call(
            @Nullable final Object data,
            @NotNull final Class<T> clazz
    ) {
        getObjectOutputStream().writeObject(data);
        @NotNull final Object result = getObjectInputStream().readObject();
        if (result instanceof AbstractResultResponse && !((AbstractResultResponse) result).getSuccess()) {
            @NotNull final AbstractResultResponse response = (AbstractResultResponse) result;
            throw new RuntimeException(response.getMessage());
        }
        return (T) result;
    }

    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(socket.getOutputStream());
    }

    private ObjectInputStream getObjectInputStream() throws IOException {
        return new ObjectInputStream(socket.getInputStream());
    }

    private OutputStream getOutputStream() throws IOException {
        return socket.getOutputStream();
    }

    private InputStream getInputStream() throws IOException {
        return socket.getInputStream();
    }

    public @Nullable Socket connect() throws IOException {
        socket = new Socket(host, port);
        return socket;
    }

    public void disconnect() throws IOException {
        if (socket == null) return;
        socket.close();
    }

}