package ru.t1.dkozoriz.tm.command.data.save;

import lombok.SneakyThrows;
import ru.t1.dkozoriz.tm.command.data.AbstractDataCommand;
import ru.t1.dkozoriz.tm.dto.request.data.save.DataBase64SaveRequest;
import ru.t1.dkozoriz.tm.dto.request.data.save.DataJsonSaveFasterRequest;

public final class DataJsonSaveFasterCommand extends AbstractDataCommand {

    public DataJsonSaveFasterCommand() {
        super("data-save-json", "save data in json file.");
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE JSON]");
        getEndpointLocator().getDomainClient().saveJsonFaster(new DataJsonSaveFasterRequest());
    }

}