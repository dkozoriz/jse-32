package ru.t1.dkozoriz.tm.command.data.load;

import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.command.data.AbstractDataCommand;
import ru.t1.dkozoriz.tm.dto.Domain;
import ru.t1.dkozoriz.tm.dto.request.data.load.DataBackupLoadRequest;
import ru.t1.dkozoriz.tm.dto.request.data.load.DataBase64LoadRequest;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    public DataBase64LoadCommand() {
        super("data-load-base64", "Load data from base64 file.");
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BASE64 LOAD]");
        getEndpointLocator().getDomainClient().loadBase64(new DataBase64LoadRequest());
    }

}