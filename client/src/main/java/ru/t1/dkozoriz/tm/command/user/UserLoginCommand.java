package ru.t1.dkozoriz.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.command.AbstractCommand;
import ru.t1.dkozoriz.tm.dto.request.user.UserLoginRequest;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractCommand {

    public UserLoginCommand() {
        super("login", "user login.");
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();

        getEndpointLocator().getAuthClient().login(new UserLoginRequest(login, password));
    }

}
