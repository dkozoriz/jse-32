package ru.t1.dkozoriz.tm.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.service.IPropertyService;

public class ConnectionClient extends AbstractClient {

    public ConnectionClient(@NotNull final IPropertyService propertyService) {
        super();
        this.setHost(propertyService.getServerHost());
        this.setPort(propertyService.getServerPort());
    }

}
