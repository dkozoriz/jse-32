package ru.t1.dkozoriz.tm.command.data.load;

import lombok.SneakyThrows;
import ru.t1.dkozoriz.tm.command.data.AbstractDataCommand;
import ru.t1.dkozoriz.tm.dto.request.data.load.DataBase64LoadRequest;
import ru.t1.dkozoriz.tm.dto.request.data.load.DataBinaryLoadRequest;

public final class DataBinaryLoadCommand extends AbstractDataCommand {

    public DataBinaryLoadCommand() {
        super("data-load-bin", "Load data from binary file.");
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BINARY LOAD]");
        getEndpointLocator().getDomainClient().loadBinary(new DataBinaryLoadRequest());
    }

}
