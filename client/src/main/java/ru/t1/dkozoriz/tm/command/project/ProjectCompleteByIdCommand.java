package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectCompleteByIdRequest;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectCompleteByIndexRequest;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    public ProjectCompleteByIdCommand() {
        super("project-complete-by-id", "complete project by id.");
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final Project project =
                getEndpointLocator().getProjectClient().projectCompleteById(new ProjectCompleteByIdRequest(id))
                        .getProject();
    }

}