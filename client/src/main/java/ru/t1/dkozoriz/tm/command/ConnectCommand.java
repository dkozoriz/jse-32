package ru.t1.dkozoriz.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.IEndpointClient;
import ru.t1.dkozoriz.tm.enumerated.Role;

import java.net.Socket;

public class ConnectCommand extends AbstractCommand {

    public ConnectCommand() {
        super("connect", "connect");
    }

    @Override
    public void execute() {
        try {
            @NotNull final IEndpointClient endpointClient = getEndpointLocator().getConnectionEndpointClient();
            @Nullable final Socket socket = endpointClient.connect();

            getEndpointLocator().getAuthClient().setSocket(socket);
            getEndpointLocator().getSystemClient().setSocket(socket);
            getEndpointLocator().getDomainClient().setSocket(socket);
            getEndpointLocator().getProjectClient().setSocket(socket);
            getEndpointLocator().getTaskClient().setSocket(socket);
            getEndpointLocator().getUserClient().setSocket(socket);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }
    }

}