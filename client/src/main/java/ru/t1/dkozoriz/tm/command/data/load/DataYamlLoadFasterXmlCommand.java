package ru.t1.dkozoriz.tm.command.data.load;

import lombok.SneakyThrows;
import ru.t1.dkozoriz.tm.command.data.AbstractDataCommand;
import ru.t1.dkozoriz.tm.dto.request.data.load.DataJsonLoadJaxBRequest;
import ru.t1.dkozoriz.tm.dto.request.data.load.DataYamlLoadFasterXmlRequest;

public final class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    public DataYamlLoadFasterXmlCommand() {
        super("data-load-yaml", "load data from yaml file.");
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD YAML]");
        getEndpointLocator().getDomainClient().loadYaml(new DataYamlLoadFasterXmlRequest());
    }

}