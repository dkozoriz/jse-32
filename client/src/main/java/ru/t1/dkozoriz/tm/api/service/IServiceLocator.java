package ru.t1.dkozoriz.tm.api.service;

public interface IServiceLocator {

    ICommandService getCommandService();

    ILoggerService getLoggerService();

    IPropertyService getPropertyService();

}