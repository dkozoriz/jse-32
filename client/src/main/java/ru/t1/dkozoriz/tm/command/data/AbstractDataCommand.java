package ru.t1.dkozoriz.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.command.AbstractCommand;
import ru.t1.dkozoriz.tm.dto.Domain;
import ru.t1.dkozoriz.tm.enumerated.Role;

public abstract class AbstractDataCommand extends AbstractCommand {

    public AbstractDataCommand(@NotNull String name, @Nullable String description) {
        super(name, description);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull

    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}