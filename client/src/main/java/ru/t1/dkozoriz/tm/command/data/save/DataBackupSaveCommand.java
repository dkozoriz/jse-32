package ru.t1.dkozoriz.tm.command.data.save;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.command.data.AbstractDataCommand;
import ru.t1.dkozoriz.tm.dto.Domain;
import ru.t1.dkozoriz.tm.dto.request.data.load.DataBackupLoadRequest;
import ru.t1.dkozoriz.tm.dto.request.data.save.DataBackupSaveRequest;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public final class DataBackupSaveCommand extends AbstractDataCommand {

    public DataBackupSaveCommand() {
        super("backup-save", "save backup.");
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BACKUP SAVE]");
        getEndpointLocator().getDomainClient().saveBackup(new DataBackupSaveRequest());
    }

}