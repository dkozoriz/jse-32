package ru.t1.dkozoriz.tm.command.system;

import ru.t1.dkozoriz.tm.dto.request.system.ServerAboutRequest;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public ApplicationAboutCommand() {
        super("about", "show developer info.", "-a");
    }

    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: " + getEndpointLocator().getSystemClient().getAbout(new ServerAboutRequest()).getName());
        System.out.println("E-mail: " + getEndpointLocator().getSystemClient().getAbout(new ServerAboutRequest()).getEmail() + "\n");

        System.out.println("[APPLICATION]");
        System.out.println("NAME: " + getEndpointLocator().getSystemClient().getAbout(new ServerAboutRequest()).getApplicationName() + "\n");

        System.out.println("[GIT]");
        System.out.println("BRANCH: " + getEndpointLocator().getSystemClient().getAbout(new ServerAboutRequest()).getBranch());
        System.out.println("COMMIT ID: " + getEndpointLocator().getSystemClient().getAbout(new ServerAboutRequest()).getCommitId());
        System.out.println("COMMITTER NAME: " + getEndpointLocator().getSystemClient().getAbout(new ServerAboutRequest()).getCommitterName());
        System.out.println("COMMITTER EMAIL: " + getEndpointLocator().getSystemClient().getAbout(new ServerAboutRequest()).getCommitterEmail());
        System.out.println("MESSAGE: " + getEndpointLocator().getSystemClient().getAbout(new ServerAboutRequest()).getMessage());
        System.out.println("TIME: " + getEndpointLocator().getSystemClient().getAbout(new ServerAboutRequest()).getTime());
    }

}