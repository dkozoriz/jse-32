package ru.t1.dkozoriz.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.endpoint.IAuthClient;
import ru.t1.dkozoriz.tm.dto.request.user.UserLoginRequest;
import ru.t1.dkozoriz.tm.dto.request.user.UserLogoutRequest;
import ru.t1.dkozoriz.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.dkozoriz.tm.dto.response.user.UserLoginResponse;
import ru.t1.dkozoriz.tm.dto.response.user.UserLogoutResponse;
import ru.t1.dkozoriz.tm.dto.response.user.UserViewProfileResponse;

@NoArgsConstructor
public final class AuthClient extends AbstractClient implements IAuthClient {

    public AuthClient(@NotNull final AbstractClient client) {
        super(client);
    }

    @NotNull
    @Override
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    public UserViewProfileResponse getProfile(@NotNull final UserViewProfileRequest request) {
        return call(request, UserViewProfileResponse.class);
    }

}