package ru.t1.dkozoriz.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.IEndpointClient;
import ru.t1.dkozoriz.tm.api.endpoint.*;

public interface IEndpointLocator {

    @NotNull
    IEndpointClient getConnectionEndpointClient();

    @NotNull
    IAuthClient getAuthClient();

    @NotNull
    IProjectClient getProjectClient();

    @NotNull
    ITaskClient getTaskClient();

    @NotNull
    IDomainClient getDomainClient();

    @NotNull
    ISystemEndpoint getSystemClient();

    @NotNull
    IUserClient getUserClient();

}