package ru.t1.dkozoriz.tm.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.endpoint.ISystemEndpoint;
import ru.t1.dkozoriz.tm.dto.request.system.ServerAboutRequest;
import ru.t1.dkozoriz.tm.dto.request.system.ServerVersionRequest;
import ru.t1.dkozoriz.tm.dto.response.project.ProjectShowListResponse;
import ru.t1.dkozoriz.tm.dto.response.system.ServerAboutResponse;
import ru.t1.dkozoriz.tm.dto.response.system.ServerVersionResponse;

public class SystemClient extends AbstractClient implements ISystemEndpoint {

    @Override
    public @NotNull ServerAboutResponse getAbout(@NotNull ServerAboutRequest request) {
        return call(request, ServerAboutResponse.class);
    }

    @Override
    public @NotNull ServerVersionResponse getVersion(@NotNull ServerVersionRequest request) {
        return call(request, ServerVersionResponse.class);
    }

}