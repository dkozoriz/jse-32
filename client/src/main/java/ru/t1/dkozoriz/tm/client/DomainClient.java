package ru.t1.dkozoriz.tm.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.endpoint.IDomainClient;
import ru.t1.dkozoriz.tm.dto.request.data.load.*;
import ru.t1.dkozoriz.tm.dto.request.data.save.*;
import ru.t1.dkozoriz.tm.dto.response.data.load.*;
import ru.t1.dkozoriz.tm.dto.response.data.save.*;

public class DomainClient extends AbstractClient implements IDomainClient {

    @NotNull
    @Override
    public DataBackupLoadResponse loadBackup(@NotNull DataBackupLoadRequest request) {
        return call(request, DataBackupLoadResponse.class);
    }

    @NotNull
    @Override
    public DataBackupSaveResponse saveBackup(@NotNull DataBackupSaveRequest request) {
        return call(request, DataBackupSaveResponse.class);
    }

    @NotNull
    @Override
    public DataBase64LoadResponse loadBase64(@NotNull DataBase64LoadRequest request) {
        return call(request, DataBase64LoadResponse.class);
    }

    @NotNull
    @Override
    public DataBase64SaveResponse saveBase64(@NotNull DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse loadBinary(@NotNull DataBinaryLoadRequest request) {
        return call(request, DataBinaryLoadResponse.class);
    }

    @NotNull
    @Override
    public DataBinarySaveResponse saveBinary(@NotNull DataBinarySaveRequest request) {
        return call(request, DataBinarySaveResponse.class);
    }

    @NotNull
    @Override
    public DataYamlLoadFasterXmlResponse loadYaml(@NotNull DataYamlLoadFasterXmlRequest request) {
        return call(request, DataYamlLoadFasterXmlResponse.class);
    }

    @NotNull
    @Override
    public DataYamlSaveFasterXmlResponse saveYaml(@NotNull DataYamlSaveFasterXmlRequest request) {
        return call(request, DataYamlSaveFasterXmlResponse.class);
    }

    @NotNull
    @Override
    public DataJsonLoadFasterResponse loadJsonFaster(@NotNull DataJsonLoadFasterRequest request) {
        return call(request, DataJsonLoadFasterResponse.class);
    }

    @NotNull
    @Override
    public DataJsonSaveFasterResponse saveJsonFaster(@NotNull DataJsonSaveFasterRequest request) {
        return call(request, DataJsonSaveFasterResponse.class);
    }

    @NotNull
    @Override
    public DataJsonLoadJaxBResponse loadJsonJaxB(@NotNull DataJsonLoadJaxBRequest request) {
        return call(request, DataJsonLoadJaxBResponse.class);
    }

    @NotNull
    @Override
    public DataJsonSaveJaxBResponse saveJsonJaxB(@NotNull DataJsonSaveJaxBRequest request) {
        return call(request, DataJsonSaveJaxBResponse.class);
    }

    @NotNull
    @Override
    public DataXmlLoadFasterXmlResponse loadXmlFaster(@NotNull DataXmlLoadFasterXmlRequest request) {
        return call(request, DataXmlLoadFasterXmlResponse.class);
    }

    @NotNull
    @Override
    public DataXmlSaveFasterXmlResponse saveXmlFaster(@NotNull DataXmlSaveFasterXmlRequest request) {
        return call(request, DataXmlSaveFasterXmlResponse.class);
    }

    @NotNull
    @Override
    public DataXmlLoadJaxBResponse loadXmlJaxB(@NotNull DataXmlLoadJaxBRequest request) {
        return call(request, DataXmlLoadJaxBResponse.class);
    }

    @NotNull
    @Override
    public DataXmlSaveJaxBResponse saveXmlJaxB(@NotNull DataXmlSaveJaxBRequest request) {
        return call(request, DataXmlSaveJaxBResponse.class);
    }

}