package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectShowListRequest;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    public ProjectChangeStatusByIdCommand() {
       super("project-change-status-by-id", "change project status by id.");
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @Nullable final Project project =
                getEndpointLocator().getProjectClient().projectChangeStatusById(new ProjectChangeStatusByIdRequest(id, status))
                        .getProject();
    }

}
