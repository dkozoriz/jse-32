package ru.t1.dkozoriz.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.endpoint.ITaskClient;
import ru.t1.dkozoriz.tm.dto.request.task.*;
import ru.t1.dkozoriz.tm.dto.response.task.*;

@NoArgsConstructor
public class TaskClient extends AbstractClient implements ITaskClient {

    public TaskClient(@NotNull final AbstractClient client) {
        super(client);
    }

    @NotNull
    @Override
    public TaskShowListResponse list(@NotNull final TaskShowListRequest request) {
        return call(request, TaskShowListResponse.class);
    }

    @NotNull
    @Override
    public TaskBindToProjectResponse taskBindToProject(@NotNull TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @NotNull
    @Override
    public TaskUnbindToProjectResponse taskUnbindToProject(@NotNull TaskUnbindToProjectRequest request) {
        return call(request, TaskUnbindToProjectResponse.class);
    }

    @NotNull
    @Override
    public TaskShowAllByProjectIdResponse taskShowAllByProjectId(@NotNull TaskShowAllByProjectIdRequest request) {
        return call(request, TaskShowAllByProjectIdResponse.class);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIdResponse taskChangeStatusById(@NotNull final TaskChangeStatusByIdRequest request) {
        return call(request, TaskChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIndexResponse taskChangeStatusByIndex(@NotNull final TaskChangeStatusByIndexRequest request) {
        return call(request, TaskChangeStatusByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskCompleteByIdResponse taskCompleteById(@NotNull final TaskCompleteByIdRequest request) {
        return call(request, TaskCompleteByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskCompleteByIndexResponse taskCompleteByIndex(@NotNull final TaskCompleteByIndexRequest request) {
        return call(request, TaskCompleteByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskStartByIdResponse taskStartById(@NotNull final TaskStartByIdRequest request) {
        return call(request, TaskStartByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskStartByIndexResponse taskStartByIndex(@NotNull final TaskStartByIndexRequest request) {
        return call(request, TaskStartByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskCreateResponse taskCreate(@NotNull final TaskCreateRequest request) {
        return call(request, TaskCreateResponse.class);
    }

    @NotNull
    @Override
    public TaskListClearResponse taskListClear(@NotNull final TaskListClearRequest request) {
        return call(request,TaskListClearResponse.class);
    }

    @NotNull
    @Override
    public TaskRemoveByIdResponse taskRemoveById(@NotNull final TaskRemoveByIdRequest request) {
        return call(request, TaskRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskRemoveByIndexResponse taskRemoveByIndex(@NotNull final TaskRemoveByIndexRequest request) {
        return call(request, TaskRemoveByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskShowByIdResponse taskShowById(@NotNull final TaskShowByIdRequest request) {
        return call(request, TaskShowByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskShowByIndexResponse taskShowByIndex(@NotNull final TaskShowByIndexRequest request) {
        return call(request, TaskShowByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskUpdateByIdResponse taskUpdateById(@NotNull final TaskUpdateByIdRequest request) {
        return call(request, TaskUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskUpdateByIndexResponse taskUpdateByIndex(@NotNull final TaskUpdateByIndexRequest request) {
        return call(request, TaskUpdateByIndexResponse.class);
    }

}