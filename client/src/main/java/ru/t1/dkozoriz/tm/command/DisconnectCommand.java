package ru.t1.dkozoriz.tm.command;

import org.jetbrains.annotations.NotNull;


public class DisconnectCommand extends AbstractCommand {

    public DisconnectCommand() {
        super("disconnect", "disconnect.");
    }

    @Override
    public void execute() {
        try {
            getEndpointLocator().getConnectionEndpointClient().disconnect();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }
    }

}