package ru.t1.dkozoriz.tm.command.data.load;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.command.data.AbstractDataCommand;
import ru.t1.dkozoriz.tm.dto.Domain;
import ru.t1.dkozoriz.tm.dto.request.data.load.DataBackupLoadRequest;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataBackupLoadCommand extends AbstractDataCommand {

    public DataBackupLoadCommand() {
        super("backup-load", "load backup.");
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BACKUP LOAD]");
        getEndpointLocator().getDomainClient().loadBackup(new DataBackupLoadRequest());
    }

}