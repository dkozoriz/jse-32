package ru.t1.dkozoriz.tm.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.endpoint.IUserClient;
import ru.t1.dkozoriz.tm.dto.request.user.*;
import ru.t1.dkozoriz.tm.dto.response.user.*;

public final class UserClient extends AbstractClient implements IUserClient {

    @NotNull
    @Override
    public UserLockResponse userLock(@NotNull UserLockRequest request) {
        return call(request, UserLockResponse.class);
    }

    @NotNull
    @Override
    public UserUnlockResponse userUnlock(@NotNull UserUnlockRequest request) {
        return call(request, UserUnlockResponse.class);
    }

    @NotNull
    @Override
    public UserRemoveResponse userRemove(@NotNull UserRemoveRequest request) {
        return call(request, UserRemoveResponse.class);
    }

    @NotNull
    @Override
    public UserChangePasswordResponse userChangePassword(@NotNull UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

    @NotNull
    @Override
    public UserUpdateProfileResponse userUpdateProfile(@NotNull UserUpdateProfileRequest request) {
        return call(request, UserUpdateProfileResponse.class);
    }

    @NotNull
    @Override
    public UserRegistryResponse userRegistry(@NotNull UserRegistryRequest request) {
        return call(request, UserRegistryResponse.class);
    }

}