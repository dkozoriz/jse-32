package ru.t1.dkozoriz.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.service.IAuthService;
import ru.t1.dkozoriz.tm.api.service.IPropertyService;
import ru.t1.dkozoriz.tm.api.service.IUserService;
import ru.t1.dkozoriz.tm.exception.user.LoginEmptyException;
import ru.t1.dkozoriz.tm.exception.user.LoginErrorException;
import ru.t1.dkozoriz.tm.exception.user.PasswordEmptyException;
import ru.t1.dkozoriz.tm.model.User;
import ru.t1.dkozoriz.tm.util.HashUtil;


public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    public AuthService(@NotNull final IUserService userService, @NotNull final IPropertyService propertyService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @NotNull
    public User registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        return userService.create(login, password, email);
    }

    public User login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new LoginErrorException();
        final boolean locked = user.getLocked();
        if (locked) throw new LoginErrorException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new LoginErrorException();
        if (!hash.equals(user.getPasswordHash())) throw new LoginErrorException();
        return user;
    }

}