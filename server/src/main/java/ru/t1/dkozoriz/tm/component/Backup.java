package ru.t1.dkozoriz.tm.component;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        load();
        executorService.scheduleWithFixedDelay(this::save, 0, 30, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

    private void save() {
        bootstrap.processCommand("backup-save", false);
    }

    private void load() {
        bootstrap.processCommand("backup-load", false);
    }

}