package ru.t1.dkozoriz.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.endpoint.IProjectEndpoint;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.dto.request.project.*;
import ru.t1.dkozoriz.tm.dto.response.project.*;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.business.Project;

import java.util.List;

public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    public ProjectShowListResponse list(@NotNull ProjectShowListRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Sort sort = request.getSort();
        @Nullable final List<Project> projectList =
                getServiceLocator().getProjectService().findAll(userId, sort);
        return new ProjectShowListResponse(projectList);
    }

    public ProjectChangeStatusByIdResponse projectChangeStatusById(@NotNull ProjectChangeStatusByIdRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Project project =
                getServiceLocator().getProjectService().changeStatusById(userId, projectId, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    public ProjectChangeStatusByIndexResponse projectChangeStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer projectIndex = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final Project project =
                getServiceLocator().getProjectService().changeStatusByIndex(userId, projectIndex, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    public ProjectCompleteByIdResponse projectCompleteById(@NotNull ProjectCompleteByIdRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final Project project =
                getServiceLocator().getProjectService().changeStatusById(userId, projectId, Status.COMPLETED);
        return new ProjectCompleteByIdResponse(project);
    }

    public ProjectCompleteByIndexResponse projectCompleteByIndex(@NotNull ProjectCompleteByIndexRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer projectIndex = request.getIndex();
        @Nullable final Project project =
                getServiceLocator().getProjectService().changeStatusByIndex(userId, projectIndex, Status.COMPLETED);
        return new ProjectCompleteByIndexResponse(project);
    }

    public ProjectStartByIdResponse projectStartById(@NotNull ProjectStartByIdRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final Project project =
                getServiceLocator().getProjectService().changeStatusById(userId, projectId, Status.IN_PROGRESS);
        return new ProjectStartByIdResponse(project);
    }

    public ProjectStartByIndexResponse projectStartByIndex(@NotNull ProjectStartByIndexRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer projectIndex = request.getIndex();
        @Nullable final Project project =
                getServiceLocator().getProjectService().changeStatusByIndex(userId, projectIndex, Status.IN_PROGRESS);
        return new ProjectStartByIndexResponse(project);
    }

    public ProjectCreateResponse projectCreate(@NotNull ProjectCreateRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project =
                getServiceLocator().getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    public ProjectListClearResponse projectListClear(@NotNull ProjectListClearRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        getServiceLocator().getProjectService().clear(userId);
        return new ProjectListClearResponse();
    }

    public ProjectRemoveByIdResponse projectRemoveById(@NotNull ProjectRemoveByIdRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getId();
        getServiceLocator().getProjectService().removeById(userId, projectId);
        return new ProjectRemoveByIdResponse();
    }

    public ProjectRemoveByIndexResponse projectRemoveByIndex(@NotNull ProjectRemoveByIndexRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer projectIndex = request.getIndex();
        getServiceLocator().getProjectService().removeByIndex(userId, projectIndex);
        return new ProjectRemoveByIndexResponse();
    }

    public ProjectShowByIdResponse projectShowById(@NotNull ProjectShowByIdRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final Project project =
                getServiceLocator().getProjectService().findById(userId, projectId);
        return new ProjectShowByIdResponse(project);
    }

    public ProjectShowByIndexResponse projectShowByIndex(@NotNull ProjectShowByIndexRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer projectIndex = request.getIndex();
        @Nullable final Project project =
                getServiceLocator().getProjectService().findByIndex(userId, projectIndex);
        return new ProjectShowByIndexResponse(project);
    }

    public ProjectUpdateByIdResponse projectUpdateById(@NotNull ProjectUpdateByIdRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project =
                getServiceLocator().getProjectService().updateById(userId, projectId, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    public ProjectUpdateByIndexResponse projectUpdateByIndex(@NotNull ProjectUpdateByIndexRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer projectIndex = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project =
                getServiceLocator().getProjectService().updateByIndex(userId, projectIndex, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }

}