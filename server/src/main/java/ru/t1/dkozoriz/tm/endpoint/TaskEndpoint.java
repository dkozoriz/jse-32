package ru.t1.dkozoriz.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.endpoint.ITaskEndpoint;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.dto.request.task.*;
import ru.t1.dkozoriz.tm.dto.response.task.*;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.business.Task;

import java.util.List;

public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    public TaskShowListResponse list(@NotNull TaskShowListRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Sort sort = request.getSort();
        @Nullable final List<Task> taskList =
                getServiceLocator().getTaskService().findAll(userId, sort);
        return new TaskShowListResponse(taskList);
    }

    @NotNull
    @Override
    public TaskBindToProjectResponse taskBindToProject(@NotNull TaskBindToProjectRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Task task =
                getServiceLocator().getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse(task);
    }

    @NotNull
    @Override
    public TaskUnbindToProjectResponse taskUnbindToProject(@NotNull TaskUnbindToProjectRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Task task =
                getServiceLocator().getProjectTaskService().unbindTaskToProject(userId, projectId, taskId);
        return new TaskUnbindToProjectResponse(task);
    }

    public TaskShowAllByProjectIdResponse taskShowAllByProjectId(@NotNull TaskShowAllByProjectIdRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final List<Task> taskList = getServiceLocator().getTaskService().findAllByProjectId(userId, projectId);
        return new TaskShowAllByProjectIdResponse(taskList);
    }


    public TaskChangeStatusByIdResponse taskChangeStatusById(@NotNull TaskChangeStatusByIdRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Task task =
                getServiceLocator().getTaskService().changeStatusById(userId, taskId, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    public TaskChangeStatusByIndexResponse taskChangeStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer taskIndex = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final Task task =
                getServiceLocator().getTaskService().changeStatusByIndex(userId, taskIndex, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    public TaskCompleteByIdResponse taskCompleteById(@NotNull TaskCompleteByIdRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getId();
        @Nullable final Task task =
                getServiceLocator().getTaskService().changeStatusById(userId, taskId, Status.COMPLETED);
        return new TaskCompleteByIdResponse(task);
    }

    public TaskCompleteByIndexResponse taskCompleteByIndex(@NotNull TaskCompleteByIndexRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer taskIndex = request.getIndex();
        @Nullable final Task task =
                getServiceLocator().getTaskService().changeStatusByIndex(userId, taskIndex, Status.COMPLETED);
        return new TaskCompleteByIndexResponse(task);
    }

    public TaskStartByIdResponse taskStartById(@NotNull TaskStartByIdRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getId();
        @Nullable final Task task =
                getServiceLocator().getTaskService().changeStatusById(userId, taskId, Status.IN_PROGRESS);
        return new TaskStartByIdResponse(task);
    }

    public TaskStartByIndexResponse taskStartByIndex(@NotNull TaskStartByIndexRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer taskIndex = request.getIndex();
        @Nullable final Task task =
                getServiceLocator().getTaskService().changeStatusByIndex(userId, taskIndex, Status.IN_PROGRESS);
        return new TaskStartByIndexResponse(task);
    }

    public TaskCreateResponse taskCreate(@NotNull TaskCreateRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task =
                getServiceLocator().getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    public TaskListClearResponse taskListClear(@NotNull TaskListClearRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        getServiceLocator().getTaskService().clear(userId);
        return new TaskListClearResponse();
    }

    public TaskRemoveByIdResponse taskRemoveById(@NotNull TaskRemoveByIdRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getId();
        getServiceLocator().getTaskService().removeById(userId, taskId);
        return new TaskRemoveByIdResponse();
    }

    public TaskRemoveByIndexResponse taskRemoveByIndex(@NotNull TaskRemoveByIndexRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer taskIndex = request.getIndex();
        getServiceLocator().getTaskService().removeByIndex(userId, taskIndex);
        return new TaskRemoveByIndexResponse();
    }

    public TaskShowByIdResponse taskShowById(@NotNull TaskShowByIdRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getId();
        @Nullable final Task task =
                getServiceLocator().getTaskService().findById(userId, taskId);
        return new TaskShowByIdResponse(task);
    }

    public TaskShowByIndexResponse taskShowByIndex(@NotNull TaskShowByIndexRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer taskIndex = request.getIndex();
        @Nullable final Task task =
                getServiceLocator().getTaskService().findByIndex(userId, taskIndex);
        return new TaskShowByIndexResponse(task);
    }

    public TaskUpdateByIdResponse taskUpdateById(@NotNull TaskUpdateByIdRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task =
                getServiceLocator().getTaskService().updateById(userId, taskId, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    public TaskUpdateByIndexResponse taskUpdateByIndex(@NotNull TaskUpdateByIndexRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer taskIndex = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task =
                getServiceLocator().getTaskService().updateByIndex(userId, taskIndex, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

}