package ru.t1.dkozoriz.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.endpoint.IDomainClient;
import ru.t1.dkozoriz.tm.api.endpoint.IDomainEndpoint;
import ru.t1.dkozoriz.tm.api.service.IDomainService;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.dto.request.data.load.*;
import ru.t1.dkozoriz.tm.dto.request.data.save.*;
import ru.t1.dkozoriz.tm.dto.response.data.load.*;
import ru.t1.dkozoriz.tm.dto.response.data.save.*;
import ru.t1.dkozoriz.tm.enumerated.Role;

public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IDomainService getDomainService() {
        return getServiceLocator().getDomainService();
    }

    @Override
    @NotNull
    public DataBackupLoadResponse loadBackup(@NotNull final DataBackupLoadRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().dataBackupLoad();
        return new DataBackupLoadResponse();
    }

    @Override
    @NotNull
    public DataBackupSaveResponse saveBackup(@NotNull final DataBackupSaveRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().dataBackupSave();
        return new DataBackupSaveResponse();
    }

    @Override
    @NotNull
    public DataBase64LoadResponse loadBase64(@NotNull final DataBase64LoadRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().dataBase64Load();
        return new DataBase64LoadResponse();
    }

    @Override
    @NotNull
    public DataBase64SaveResponse saveBase64(@NotNull final DataBase64SaveRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().dataBase64Save();
        return new DataBase64SaveResponse();
    }

    @Override
    @NotNull
    public DataBinaryLoadResponse loadBinary(@NotNull final DataBinaryLoadRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().dataBinaryLoad();
        return new DataBinaryLoadResponse();
    }

    @Override
    @NotNull
    public DataBinarySaveResponse saveBinary(@NotNull final DataBinarySaveRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().dataBinarySave();
        return new DataBinarySaveResponse();
    }

    @Override
    @NotNull
    public DataYamlLoadFasterXmlResponse loadYaml(@NotNull final DataYamlLoadFasterXmlRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().dataYamlLoad();
        return new DataYamlLoadFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataYamlSaveFasterXmlResponse saveYaml(@NotNull final DataYamlSaveFasterXmlRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().dataYamlSave();
        return new DataYamlSaveFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataJsonLoadFasterResponse loadJsonFaster(@NotNull final DataJsonLoadFasterRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().dataJsonLoadFaster();
        return new DataJsonLoadFasterResponse();
    }

    @Override
    @NotNull
    public DataJsonSaveFasterResponse saveJsonFaster(@NotNull final DataJsonSaveFasterRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().dataJsonSaveFaster();
        return new DataJsonSaveFasterResponse();
    }

    @Override
    @NotNull
    public DataJsonLoadJaxBResponse loadJsonJaxB(@NotNull final DataJsonLoadJaxBRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().dataJsonLoadJaxB();
        return new DataJsonLoadJaxBResponse();
    }

    @Override
    @NotNull
    public DataJsonSaveJaxBResponse saveJsonJaxB(@NotNull final DataJsonSaveJaxBRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().dataJsonSaveJaxB();
        return new DataJsonSaveJaxBResponse();
    }

    @Override
    @NotNull
    public DataXmlLoadFasterXmlResponse loadXmlFaster(@NotNull final DataXmlLoadFasterXmlRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().dataXmlLoadFasterXml();
        return new DataXmlLoadFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataXmlSaveFasterXmlResponse saveXmlFaster(@NotNull final DataXmlSaveFasterXmlRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().dataXmlSaveFasterXml();
        return new DataXmlSaveFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataXmlLoadJaxBResponse loadXmlJaxB(@NotNull final DataXmlLoadJaxBRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().dataXmlLoadJaxB();
        return new DataXmlLoadJaxBResponse();
    }

    @Override
    @NotNull
    public DataXmlSaveJaxBResponse saveXmlJaxB(@NotNull final DataXmlSaveJaxBRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().dataXmlSaveJaxB();
        return new DataXmlSaveJaxBResponse();
    }

}