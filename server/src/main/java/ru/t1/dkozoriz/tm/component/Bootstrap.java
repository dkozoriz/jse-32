package ru.t1.dkozoriz.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.endpoint.*;
import ru.t1.dkozoriz.tm.api.repository.IUserRepository;
import ru.t1.dkozoriz.tm.api.repository.business.IProjectRepository;
import ru.t1.dkozoriz.tm.api.repository.business.ITaskRepository;
import ru.t1.dkozoriz.tm.api.service.*;
import ru.t1.dkozoriz.tm.api.service.business.IProjectService;
import ru.t1.dkozoriz.tm.api.service.business.ITaskService;
import ru.t1.dkozoriz.tm.dto.request.data.load.*;
import ru.t1.dkozoriz.tm.dto.request.data.save.*;
import ru.t1.dkozoriz.tm.dto.request.project.*;
import ru.t1.dkozoriz.tm.dto.request.system.ServerAboutRequest;
import ru.t1.dkozoriz.tm.dto.request.system.ServerVersionRequest;
import ru.t1.dkozoriz.tm.dto.request.task.*;
import ru.t1.dkozoriz.tm.dto.request.user.*;
import ru.t1.dkozoriz.tm.endpoint.*;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.User;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.repository.UserRepository;
import ru.t1.dkozoriz.tm.repository.business.ProjectRepository;
import ru.t1.dkozoriz.tm.repository.business.TaskRepository;
import ru.t1.dkozoriz.tm.service.*;
import ru.t1.dkozoriz.tm.service.business.ProjectService;
import ru.t1.dkozoriz.tm.service.business.TaskService;
import ru.t1.dkozoriz.tm.util.SystemUtil;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService(propertyService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);

        server.registry(ProjectShowListRequest.class, projectEndpoint::list);
        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::projectChangeStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::projectChangeStatusByIndex);
        server.registry(ProjectCompleteByIdRequest.class, projectEndpoint::projectCompleteById);
        server.registry(ProjectCompleteByIndexRequest.class, projectEndpoint::projectCompleteByIndex);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::projectRemoveById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::projectRemoveByIndex);
        server.registry(ProjectCreateRequest.class, projectEndpoint::projectCreate);
        server.registry(ProjectListClearRequest.class, projectEndpoint::projectListClear);
        server.registry(ProjectShowByIdRequest.class, projectEndpoint::projectShowById);
        server.registry(ProjectShowByIndexRequest.class, projectEndpoint::projectShowByIndex);
        server.registry(ProjectStartByIdRequest.class, projectEndpoint::projectStartById);
        server.registry(ProjectStartByIndexRequest.class, projectEndpoint::projectStartByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::projectUpdateById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::projectUpdateByIndex);

        server.registry(TaskShowListRequest.class, taskEndpoint::list);
        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::taskChangeStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::taskChangeStatusByIndex);
        server.registry(TaskCompleteByIdRequest.class, taskEndpoint::taskCompleteById);
        server.registry(TaskCompleteByIndexRequest.class, taskEndpoint::taskCompleteByIndex);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::taskRemoveById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::taskRemoveByIndex);
        server.registry(TaskCreateRequest.class, taskEndpoint::taskCreate);
        server.registry(TaskListClearRequest.class, taskEndpoint::taskListClear);
        server.registry(TaskShowByIdRequest.class, taskEndpoint::taskShowById);
        server.registry(TaskShowByIndexRequest.class, taskEndpoint::taskShowByIndex);
        server.registry(TaskStartByIdRequest.class, taskEndpoint::taskStartById);
        server.registry(TaskStartByIndexRequest.class, taskEndpoint::taskStartByIndex);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::taskUpdateById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::taskUpdateByIndex);

        server.registry(DataBackupLoadRequest.class, domainEndpoint::loadBackup);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::saveBackup);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::loadBase64);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::saveBase64);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::loadBinary);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::saveBinary);
        server.registry(DataJsonLoadFasterRequest.class, domainEndpoint::loadJsonFaster);
        server.registry(DataJsonSaveFasterRequest.class, domainEndpoint::saveJsonFaster);
        server.registry(DataJsonLoadJaxBRequest.class, domainEndpoint::loadJsonJaxB);
        server.registry(DataJsonSaveJaxBRequest.class, domainEndpoint::saveJsonJaxB);
        server.registry(DataXmlLoadFasterXmlRequest.class, domainEndpoint::loadXmlFaster);
        server.registry(DataXmlSaveFasterXmlRequest.class, domainEndpoint::saveXmlFaster);
        server.registry(DataXmlLoadJaxBRequest.class, domainEndpoint::loadXmlJaxB);
        server.registry(DataXmlSaveJaxBRequest.class, domainEndpoint::saveXmlJaxB);
        server.registry(DataYamlLoadFasterXmlRequest.class, domainEndpoint::loadYaml);
        server.registry(DataYamlSaveFasterXmlRequest.class, domainEndpoint::saveYaml);

        server.registry(UserUpdateProfileRequest.class, userEndpoint::userUpdateProfile);
        server.registry(UserLockRequest.class, userEndpoint::userLock);
        server.registry(UserUnlockRequest.class, userEndpoint::userUnlock);
        server.registry(UserRegistryRequest.class, userEndpoint::userRegistry);
        server.registry(UserRemoveRequest.class, userEndpoint::userRemove);
        server.registry(UserChangePasswordRequest.class, userEndpoint::userChangePassword);

    }

    private void initBackup() {
        backup.start();
    }


    private void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, boolean checkRoles) {
        if (command == null || command.isEmpty()) return;
    }


    private void initDemoData() {
        @NotNull final User user1 = userService.create("user1", "password1", "user1@user");
        @NotNull final User user2 = userService.create("user2", "password2", "user2@user");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        @NotNull final Project project1 = new Project("Project1", Status.IN_PROGRESS);
        @NotNull final Project project2 = new Project("Project2", Status.IN_PROGRESS);
        @NotNull final Project project3 = new Project("Project3", Status.COMPLETED);
        @NotNull final Project project4 = new Project("Project4", Status.COMPLETED);
        @NotNull final Project project5 = new Project("Project5", Status.COMPLETED);
        @NotNull final Project project6 = new Project("Project6", Status.IN_PROGRESS);
        projectRepository.add(user1.getId(), project1);
        projectRepository.add(user1.getId(), project2);
        projectRepository.add(user1.getId(), project3);
        projectRepository.add(user2.getId(), project4);
        projectRepository.add(user2.getId(), project5);
        projectRepository.add(user2.getId(), project6);

        taskService.create(user1.getId(), "task1_1", "description", project1.getId());
        taskService.create(user1.getId(), "task1_2", "description", project1.getId());
        taskService.create(user1.getId(), "task2_1", "description", project2.getId());
        taskService.create(user1.getId(), "task3_1", "description", project3.getId());
        taskService.create(user1.getId(), "task3_2", "description", project3.getId());
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }


    private void prepareStartup() {
        loggerService.info("*** TASK MANAGER SERVER IS STARTED ***");
        initPID();
        initDemoData();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        initBackup();
        server.start();
    }

    private void prepareShutdown() {
        backup.stop();
        loggerService.info("** TASK MANAGER SERVER IS SHUTTING DOWN **");
        server.stop();
    }


    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @Nullable final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]\n");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]\n");
            }
        }
    }

    public void run(@Nullable final String... args) {
        prepareStartup();
        processCommands();
    }

}