package ru.t1.dkozoriz.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.IEndpointClient;
import ru.t1.dkozoriz.tm.dto.request.project.*;
import ru.t1.dkozoriz.tm.dto.response.project.*;

public interface IProjectClient extends IEndpointClient {

    @NotNull
    ProjectShowListResponse list(@NotNull ProjectShowListRequest request);

    @NotNull
    ProjectChangeStatusByIdResponse projectChangeStatusById(@NotNull ProjectChangeStatusByIdRequest request);

    @NotNull
    ProjectChangeStatusByIndexResponse projectChangeStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request);

    @NotNull
    ProjectCompleteByIdResponse projectCompleteById(@NotNull ProjectCompleteByIdRequest request);

    @NotNull
    ProjectCompleteByIndexResponse projectCompleteByIndex(@NotNull ProjectCompleteByIndexRequest request);

    @NotNull
    ProjectStartByIdResponse projectStartById(@NotNull ProjectStartByIdRequest request);

    @NotNull
    ProjectStartByIndexResponse projectStartByIndex(@NotNull ProjectStartByIndexRequest request);

    @NotNull
    ProjectCreateResponse projectCreate(@NotNull ProjectCreateRequest request);

    @NotNull
    ProjectListClearResponse projectListClear(@NotNull ProjectListClearRequest request);

    @NotNull
    ProjectRemoveByIdResponse projectRemoveById(@NotNull ProjectRemoveByIdRequest request);

    @NotNull
    ProjectRemoveByIndexResponse projectRemoveByIndex(@NotNull ProjectRemoveByIndexRequest request);

    @NotNull
    ProjectShowByIdResponse projectShowById(@NotNull ProjectShowByIdRequest request);

    @NotNull
    ProjectShowByIndexResponse projectShowByIndex(@NotNull ProjectShowByIndexRequest request);

    @NotNull
    ProjectUpdateByIdResponse projectUpdateById(@NotNull ProjectUpdateByIdRequest request);

    @NotNull
    ProjectUpdateByIndexResponse projectUpdateByIndex(@NotNull ProjectUpdateByIndexRequest request);

}