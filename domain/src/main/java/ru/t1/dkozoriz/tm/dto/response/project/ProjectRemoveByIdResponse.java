package ru.t1.dkozoriz.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public class ProjectRemoveByIdResponse extends AbstractResponse {
}
