package ru.t1.dkozoriz.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.IEndpointClient;
import ru.t1.dkozoriz.tm.dto.request.task.*;
import ru.t1.dkozoriz.tm.dto.response.task.*;

public interface ITaskClient extends IEndpointClient {

    @NotNull
    TaskShowListResponse list(@NotNull TaskShowListRequest request);

    @NotNull
    TaskBindToProjectResponse taskBindToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull
    TaskUnbindToProjectResponse taskUnbindToProject(@NotNull TaskUnbindToProjectRequest request);

    @NotNull
    TaskShowAllByProjectIdResponse taskShowAllByProjectId(@NotNull TaskShowAllByProjectIdRequest request);

    @NotNull
    TaskChangeStatusByIdResponse taskChangeStatusById(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull
    TaskChangeStatusByIndexResponse taskChangeStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request);

    @NotNull
    TaskCompleteByIdResponse taskCompleteById(@NotNull TaskCompleteByIdRequest request);

    @NotNull
    TaskCompleteByIndexResponse taskCompleteByIndex(@NotNull TaskCompleteByIndexRequest request);

    @NotNull
    TaskStartByIdResponse taskStartById(@NotNull TaskStartByIdRequest request);

    @NotNull
    TaskStartByIndexResponse taskStartByIndex(@NotNull TaskStartByIndexRequest request);

    @NotNull
    TaskCreateResponse taskCreate(@NotNull TaskCreateRequest request);

    @NotNull
    TaskListClearResponse taskListClear(@NotNull TaskListClearRequest request);

    @NotNull
    TaskRemoveByIdResponse taskRemoveById(@NotNull TaskRemoveByIdRequest request);

    @NotNull
    TaskRemoveByIndexResponse taskRemoveByIndex(@NotNull TaskRemoveByIndexRequest request);

    @NotNull
    TaskShowByIdResponse taskShowById(@NotNull TaskShowByIdRequest request);

    @NotNull
    TaskShowByIndexResponse taskShowByIndex(@NotNull TaskShowByIndexRequest request);

    @NotNull
    TaskUpdateByIdResponse taskUpdateById(@NotNull TaskUpdateByIdRequest request);

    @NotNull
    TaskUpdateByIndexResponse taskUpdateByIndex(@NotNull TaskUpdateByIndexRequest request);

}