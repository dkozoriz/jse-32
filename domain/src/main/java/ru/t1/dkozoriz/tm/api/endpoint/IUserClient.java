package ru.t1.dkozoriz.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.IEndpointClient;
import ru.t1.dkozoriz.tm.dto.request.user.*;
import ru.t1.dkozoriz.tm.dto.response.user.*;

public interface IUserClient extends IEndpointClient {

    @NotNull
    UserLockResponse userLock(@NotNull UserLockRequest request);

    @NotNull
    UserUnlockResponse userUnlock(@NotNull UserUnlockRequest request);

    @NotNull
    UserRemoveResponse userRemove(@NotNull UserRemoveRequest request);

    @NotNull
    UserChangePasswordResponse userChangePassword(@NotNull UserChangePasswordRequest request);

    @NotNull
    UserUpdateProfileResponse userUpdateProfile(@NotNull UserUpdateProfileRequest request);

    @NotNull
    UserRegistryResponse userRegistry(@NotNull UserRegistryRequest request);

}