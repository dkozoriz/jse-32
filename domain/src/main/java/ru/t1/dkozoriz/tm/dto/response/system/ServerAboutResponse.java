package ru.t1.dkozoriz.tm.dto.response.system;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.response.AbstractResponse;

@Getter
@Setter
public final class ServerAboutResponse extends AbstractResponse {

    @Nullable
    private String email;

    @Nullable
    private String name;

    @Nullable
    private String applicationName;

    @Nullable
    private String branch;

    @Nullable
    private String commitId;

    @Nullable
    private String committerName;

    @Nullable
    private String committerEmail;

    @Nullable
    private String message;

    @Nullable
    private String time;

}