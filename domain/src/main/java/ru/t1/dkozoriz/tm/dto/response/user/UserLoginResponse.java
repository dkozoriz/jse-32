package ru.t1.dkozoriz.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
@NoArgsConstructor
public class UserLoginResponse extends AbstractResultResponse {

    public UserLoginResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}