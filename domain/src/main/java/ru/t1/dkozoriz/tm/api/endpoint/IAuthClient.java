package ru.t1.dkozoriz.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.IEndpointClient;
import ru.t1.dkozoriz.tm.dto.request.user.UserLoginRequest;
import ru.t1.dkozoriz.tm.dto.request.user.UserLogoutRequest;
import ru.t1.dkozoriz.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.dkozoriz.tm.dto.response.user.UserLoginResponse;
import ru.t1.dkozoriz.tm.dto.response.user.UserLogoutResponse;
import ru.t1.dkozoriz.tm.dto.response.user.UserViewProfileResponse;

public interface IAuthClient extends IEndpointClient {
    @NotNull
    UserLoginResponse login(@NotNull UserLoginRequest request);

    @NotNull
    UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    @NotNull
    UserViewProfileResponse getProfile(@NotNull UserViewProfileRequest request);

}